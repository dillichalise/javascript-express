const mongodb = require('mongodb');


const MongoClient = mongodb.MongoClient;
const conxnURL = 'mongodb://localhost:27017';
const dbName = 'learnjs';
const colName = 'users';

module.exports = {
    mongodb, MongoClient, conxnURL, dbName, colName
}