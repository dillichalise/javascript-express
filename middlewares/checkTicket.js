module.exports = function (req, res, next) {
    //check ticket
    console.log('req params>>>', req.params);
    const { ticket } = req.query;
    if(!ticket) {
        return next({
            msg: 'Ticket Not Found!!'
        })
    }
    console.log('i am scond app level middleware i will be present in every http req res cycle.');
    return next();
}