var express = require('express');
var morgan = require('morgan');
var path = require('path');
var config = require('./config');

var app = express();

require('./db')
//load routing level middleware
var api = require('./controllers/api.route')()

// load third party
app.use(morgan('dev'));

//inbuilt middleware
app.use('/file', express.static(path.join(__dirname, 'files')));
app.use(express.urlencoded({ extended: true }))
app.use(express.json());

app.use('/api', api);

// for favIcon
app.use(function (req, res, next) {
    //404 error handler
    console.log('i am app level  middleware');
    next({
        msg: 'Not Found',
        status: 404
    });

})


// error handling middleware
app.use(function (err, req, res, next) {
    console.log('i am error handling middleware', err);
    res.status(err.status || 400);
    res.json({
        message: err.msg || err,
        status: err.status || 400,
    });
});

// Server
app.listen(config.port, function (err, done) {
    if (err) {
        console.log('error listening to port');
    } else {
        console.log('server listening to port', config.port);
        console.log('press Ctrl + C to exit');
    }
})
