//db connections

const mongoose = require('mongoose');
const { conxnURL, dbName } = require('./config/db.config');

mongoose.connect(`${conxnURL}/${dbName}`, { useNewUrlParser: true }, function (err, done) {
    if (err) {
        console.log('db connection failed');
    } else {
        console.log('db connection success');
    }
})