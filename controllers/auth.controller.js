const UserModel = require('./../models/user.model');
const map_user = require('./../helpers/map_user_request');
const jwt = require('jsonwebtoken');
const config = require('./../config');

function login(req, res, next) {
    console.log('form login request here>>', req.body);
    UserModel.findOne({
        username: req.body.username,
    })
        // .sort({
        //     _id: -1
        // })
        // .limit(2) // show first 2 data
        // .skip(2) // skips first 2 data
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                var hashPassword = user.password;
                var userPassword = req.body.password;

                if (hashPassword == userPassword) {
                    var token = jwt.sign({ id: user._id, name: user.username, role: user.role }, config.jwtSecret);
                    res.json({
                        user,
                        token
                    });
                } else {
                    console.log('invalid password');
                    next({
                        msg: 'invalid credentials'
                    })
                }
            } else {
                next({
                    msg: 'invalid credentials'
                })
            }
        })
}

function register(req, res, next) {
    console.log('req body data>>>', req.body);

    let newUser = new UserModel({});
    map_user(newUser, req.body);

    console.log('new user>>>', newUser);


    newUser.save()
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        });

}

module.exports = {
    login, register
}