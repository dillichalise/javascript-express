var router = require('express').Router();
const authorize = require("./../middlewares/authorize");
const UserModel = require('./../models/user.model');
var map_user = require('./../helpers/map_user_request');
router.route('/')
    .get(function (req, res, next) {
        console.log('req.user>>>', req.user);
        var condition = {};

        var currentPage = Number(req.query.page) - 1 || 0;
        var pageCount = Number(req.query.pageCount) || 5;
        var skipValue = (currentPage * pageCount);

        // console.log('skip value>>>', skipValue);
        // console.log('page count>>', pageCount);

        UserModel.find(condition)
            .sort({ _id: -1 })
            .skip(skipValue)
            .limit(pageCount)
            .exec(function (err, users) {
                if (err) {
                    return next(err);
                }
                res.status(200).json(users);
            })
    })

router.route('/change-password')
    .get(function (req, res, next) {

    })
    .post(function (req, res, next) {
        console.log('post here');
        res.end('hi');

    })
    .put(function (req, res, next) {

    })
    .delete(function (req, res, next) {

    });

router.route('/:id')
    .get(function (req, res, next) {
        console.log('req.user>>>', req.user);
        // UserModel.findOne({ _id: req.params.id })
        //     .then(function (user) {
        //         res.status(200).json(user);
        //     })
        //     .catch(function (err) {
        //         next(err);
        //     });
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            res.status(200).json(user);
        })
    })

    .put(function (req, res, next) {
        console.log('req.user>>>', req.user);
        var id = req.params.id;
        UserModel.findById(id)
            .exec(function (err, user) {
                if (err) {
                    return next(err);
                }
                if (!user) {
                    return next({
                        msg: 'user not found'
                    })
                }
                map_user(user, req.body);

                user.updatedBy = req.user.name;
                user.save(function (err, done) {
                    if (err) {
                        return next(err);
                    }
                    res.status(200).json(done);
                });
            })

    })
    .delete(authorize, function (req, res, next) {
        console.log('req.user>>>', req.user);

        // UserModel.findByIdAndDelete(req.params.id, function (err, user) {
        //     if (err) {
        //         return next(err);
        //     }
        //     res.status(200).json(user);
        // })
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                user.remove(function (err, done) {
                    if (err) {
                        return next(err);
                    }
                    res.status(200).json(done);
                })

            } else {
                next({
                    msg: 'user not found'
                })
            }
        })
    })



module.exports = router;