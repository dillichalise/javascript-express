var express = require('express');
var router = express.Router();
const authCtrl = require('./auth.controller');

router.get('/', function (req, res, next) {
    console.log('i am at empty route of auth file');
    res.render('login.pug', {
        name: 'dilli',
        title: 'hello'
    })
});

router.get('/hello', function (req, res, next) {
    res.send('i am at hello route of auth file');
});

router.post('/', function (req, res, next) {
    // form data here
    console.log('form login request>>', req.body);
    // find user information from db
    res.redirect('/user');
})

router.post('/login', authCtrl.login);

router.post('/register', authCtrl.register)

module.exports = router;