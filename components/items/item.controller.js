const ItemQuery = require('./item.query');
const fs = require('fs');
const path = require('path');
// __dirname == working directory path
// process.cwd() root directory path
function insert(req, res, next) {
    console.log('req.body', req.body);
    console.log('req.file>>>', req.file)
    if (req.fileError) {
        return next({
            msg: 'invalid file format from filter'
        })
    }
    if (req.file) {
        var mimeType = req.file.mimetype.split('/')[0];
        if (mimeType != 'image') {
            fs.unlink(path.join(process.cwd(), 'uploads/' + req.file.filename), function (err, done) {
                if (err) {
                    console.log('err ');
                } else {
                    console.log('removed');
                }
            })
            return next({
                msg: "invalid file format"
            })
        }
    }
    var data = req.body;
    data.user = req.user._id;
    if (req.file) {
        data.image = req.file.filename;
    }
    ItemQuery.insert(data)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        });

}
function find(req, res, next) {
    let condition = {};
    if (req.user.role != 1) {
        condition.user = req.user._id;
    }
    ItemQuery.fetch(condition)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        });

}
function findById(req, res, next) {
    ItemQuery.fetch({ _id: req.params.id })
        .then(function (data) {
            res.json(data[0]);
        })
        .catch(function (err) {
            next(err);
        });

}
function update(req, res, next) {
    console.log('req.file>>>', req.file)
    console.log('req.body>>>', req.body);
    if (req.file) {
        req.body.image = req.file.filename;
    }
    ItemQuery.update(req.params.id, req.body)
        .then(function (response) {
            if (req.file) {
                fs.unlink(path.join(process.cwd(), 'uploads/' + response.oldImage), function (err, removed) {
                    if (err) {
                        console.log('err')
                    } else {
                        console.log('removed')
                    }
                })
            }
            res.json(response.data);
        })
        .catch(function (err) {
            next(err);
        });

}
function remove(req, res, next) {
    ItemQuery.remove(req.params.id)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        });

}

module.exports = {
    insert, find, findById, update, remove
};
