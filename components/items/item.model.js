const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ratingSchema = new Schema({
    point: Number,
    message: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
}, {
    timestamps: true
})
const ItemSchema = new Schema({
    name: {
        type: String,
        required: true,
        lowercase: true,
        trim: true
    },
    category: {
        type: String,
        required: true
    },
    description: {
        type: String,
    },
    brand: String,
    productCode: String,
    color: {
        type: String
    },
    price: Number,
    discount: {
        status: Boolean,
        discountType: {
            type: String,
            enum: ['percentage', 'value', 'qty']
        },
        discountValue: String, //10% 200 4
    },
    status: {
        type: String,
        enum: ['out of stock', 'availabel', 'sold']
    },
    quantity: Number,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    reviews: [ratingSchema],
    tags: [String],
    image: String
}, {
    timestamps: true
});

const ItemModel = mongoose.model('item', ItemSchema);
module.exports = ItemModel;
