const router = require('express').Router();
const ItemCtrl = require('./item.controller');
const authorize = require('./../../middlewares/authorize');
const multer = require('multer');
// const upload = multer({ dest: 'uploads' })
var myStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads');
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname);
    }
});

function filter(req, file, cb) {
    var mimeType = file.mimetype.split('/')[0];
    if (mimeType != 'image') {
        req.fileError = 'true';
        cb(null, false)
    } else {
        cb(null, true)
    }
}
var upload = multer({
    storage: myStorage,
    fileFilter: filter
})
router.get('/', ItemCtrl.find);
router.post('/', upload.single('file'), ItemCtrl.insert);
router.route('/:id')
    .get(ItemCtrl.findById)
    .put(upload.single('file'), ItemCtrl.update)
    .delete(ItemCtrl.remove);

module.exports = router;
